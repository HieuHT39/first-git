<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%-- <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet"/> --%>
<title>New Customer</title>
<style>
	.error{
	color: red;
	font-style: italic;
	}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/GiaoDien.jsp"></jsp:include>

<h3 align="center" style="color: rgb(21, 255, 0);">DANG KY VE XE</h3>

<div align="center">
		<form:form action="${pageContext.request.contextPath}/datVe/save-datVe" method="POST" modelAttribute="datVeForm">
			<table>
				<tr>
					<td><form:label path="maDV">Ma DV</form:label></td>
					<td><form:input path="maDV" name="maDV" id="maDV" /></td>
					<td><form:errors path="maDV" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="CCCD.CCCD">CCCD</form:label></td>
					<td><form:input path="CCCD.CCCD" name="CCCD.CCCD" id="CCCD.CCCD" /></td>
					<td><form:errors path="CCCD.CCCD" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="ngayDatVe" >Ngay dat ve</form:label></td>
					<td><form:input type="date" path="ngayDatVe" name="ngayDatVe" id="ngayDatVe" /></td>
					<td><form:errors path="ngayDatVe" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="bienSoXe">Bien so xe</form:label></td>
					<td><form:input path="bienSoXe" name="bienSoXe" id="bienSoXe" /></td>
					<td><form:errors path="bienSoXe" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="diaDiemXuatPhat">Dia diem xuat phat</form:label></td>
					<td><form:input path="diaDiemXuatPhat" name="diaDiemXuatPhat" id="diaDiemXuatPhat" /></td>
					<td><form:errors path="diaDiemXuatPhat" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="diaDiemDen">Dia diem den</form:label></td>
					<td><form:input path="diaDiemDen" name="diaDiemDen" id="diaDiemDen"/></td>
					<td><form:errors path="diaDiemDen" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="ngayXuatPhat" >Ngay xuat phat</form:label></td>
					<td><form:input type="date" path="ngayXuatPhat" name="ngayXuatPhat" id="ngayXuatPhat" /></td>
					<td><form:errors path="ngayXuatPhat" cssClass="error"/></td>
				</tr>
				
				<tr>
					<td></td>
					<td><form:button value="Add">Luu Dang Ky</form:button></td>
				</tr>
			</table>
		</form:form>
		</div>

<script src="<c:url value="/resources/js/general.js" />"></script>
</body>
</html>