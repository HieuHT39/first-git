<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%-- <%@taglib uri="http://www.springframework.org/tags" prefix="s" %> --%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="/JAVA05_FEE_ASS1_HIEUHT29/fontawesome-free-6.2.0-web/fontawesome-free-6.2.0-web/css/all.min.css">
<style>
.pagination {
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
	border: 1px solid #ddd;
	margin: 0 4px;
}

.pagination a.active {
	background-color: #4CAF50;
	color: white;
	border: 1px solid #4CAF50;
}

.pagination a:hover:not(.active) {
	background-color: #ddd;
}

table, td, th {
	border: 1px solid black;
}

table {
	border-collapse: collapse;
	width: 50%;
}

td {
	text-align: center;
}
.khung{
	height: 750px;
	width: 1550px;
}
</style>
<title>New san</title>
</head>
<body>
<!-- link dường dẫn tới view giao diện  -->
<jsp:include page="/WEB-INF/jsp/GiaoDien.jsp"></jsp:include>

<div class="khung" align="center" style="background-color: rgb(234, 246, 213);">
	<!-- Tao button tao moi khach hang -->
<button ><a href="${pageContext.request.contextPath}/khachHang/ThemKhachHang">Tao moi khach hang</a></button>
<!-- Tạo tiêu đề  -->
<h2 style="color: red;" >Danh Sach Khach Hnag</h2>
<!-- Tạo find tìm kiếm  -->
<form action="${pageContext.request.contextPath}/khachHang/tim-khachHang " method="get" modelAttribute="hienThiKhachHang">
<select name="Filter">
<option value="trangThaiTiem">Trang Thai Tiem</option>
<option value="ketQuaXetNghiem">Ket qua xet nghiem</option>
</select>
	<input type="text" name="searchName" placeholder="enter name to search">
	<button style="background-color: rgb(112, 221, 76);">Find</button>
	<!-- Tạo bảng để hiển thị thông tin  -->
</form>
<br>

			<table>
				<thead>
				<tr>
					<th width="20%">CCCD</th>
					<th width="20%">Ten khach hang</th>
					<th width="20%">Ngay sinh</th>
					<th width="20%">Gioi Tinh</th>
					<th width="20%">So dien thoai</th>
					<th width="20%">Trang thai tiem</th>
					<th width="20%">Ket qua xet nghiem</th>
					<th width="20%">Ngay xet nghiem</th>
				</tr>
				</thead>
				<tbody>
<!-- 				đặt tên items để sử dungj ở class KhachHangController -->
					<c:forEach var="khachHang" items="${khachHangs}">
						<tr>
							<td>${khachHang.CCCD}</td>
							<td>${khachHang.hoTen}</td>
							<td>${khachHang.ngaySinh}</td>
							<td>${khachHang.gioiTinh}</td>
							<td>${khachHang.soDienThoai}</td>
							<td>${khachHang.trangThaiTiem}</td>
							<td>${khachHang.ketQuaXetNghiem}</td>
							<td>${khachHang.ngayXetNghiem}</td>
							<td>
		
							<a href="delete-khachHang?id=${khachHang.CCCD}"><button type="submit" class="btn btn-danger px-4" style="color:rgb(153, 15, 238) ;">
								<i class="fas fa-trash " ></i></button></a> 
							<a href="update-khachHang/${khachHang.CCCD}"><button type="submit" class="btn btn-danger px-4" style="color:red ;">
								<i class="fas fa-pen " ></i></button></a> 
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<br/>
			<!-- Tạo button chuyển trang -->
	<div class="pagination">
			<c:if test="${currentPage > 1}">
				<a href="HienThiKhachHang?page=${currentPage-1}">Previous</a>
			</c:if>
			<c:forEach begin="1" end="${totalPages}" var="i">
				<c:choose>
					<c:when test="${currentPage eq i}">
						<a class="active"> ${i} </a>
					</c:when>
					<c:otherwise>
						<a href="HienThiKhachHang?page=${i}">${i}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>

			<c:if test="${currentPage lt totalPages}">
				<a href="HienThiKhachHang?page=${currentPage+1}">Next</a>
			</c:if>
	</div>
			
</div>
</body>
</html>