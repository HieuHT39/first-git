<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%-- <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet"/> --%>
<style>
.pagination {
	display: inline-block;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
	border: 1px solid #ddd;
	margin: 0 4px;
}

.pagination a.active {
	background-color: #4CAF50;
	color: white;
	border: 1px solid #4CAF50;
}

.pagination a:hover:not(.active) {
	background-color: #ddd;
}

table, td, th {
	border: 1px solid black;
}

table {
	border-collapse: collapse;
	width: 50%;
}

td {
	text-align: center;
}
</style>
<title>New Machine</title>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/GiaoDien.jsp"></jsp:include>

<div align="center">
<%int i = 0; %>
<h2>HIEN THI THONG TIN DAT VE</h2>


			<table class="table table-bordered table-hover bg-light table-striped">
			<thead>
				<tr>
					<th width="20%">Ma dat ve</th>
					<th width="20%">CCCD</th>
					<th width="20%">Ngay dat ve</th>
					<th width="20%">Bien so xe</th>
					<th width="20%">Dia diem xuat phat</th>
					<th width="20%">Dia diem den</th>
					<th width="20%">Ngay xuat phat</th>
				</tr>
			</thead>
			<tbody id="myTable">
				<c:forEach var="datVe" items="${dv}">
					<tr>
					
						<td>${datVe.maDV}</td>
						<td>${datVe.CCCD.CCCD}</td>
						<td>${datVe.ngayDatVe}</td>
						<td>${datVe.bienSoXe}</td>
						<td>${datVe.diaDiemXuatPhat}</td>
						<td>${datVe.diaDiemDen}</td>
						<td>${datVe.ngayXuatPhat}</td>

						<td><a href="delete?id=${datVe.maDV}">Delete</a> <a
							href="update/${datVe.maDV}">Update</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
			<br/>
			
	<div class="pagination">
			<c:if test="${currentPage > 1}">
				<a href="listinfo?page=${currentPage-1}">Previous</a>
			</c:if>
			<c:forEach begin="1" end="${totalPages}" var="i">
				<c:choose>
					<c:when test="${currentPage eq i}">
						<a class="active"> ${i} </a>
					</c:when>
					<c:otherwise>
						<a href="listinfo?page=${i}">${i}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>

			<c:if test="${currentPage lt totalPages}">
				<a href="listinfo?page=${currentPage+1}">Next</a>
			</c:if>
	</div>
			
</div>

</body>
</html>