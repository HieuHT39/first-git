<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=], initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    />
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <title>Document</title>
</head>
<body>
<div class="container-fluid px-5 " style="background-color: rgb(194, 212, 129) ;"> 
    <div class="row pt-3">
        <a class="navbar-brand " href="${pageContext.request.contextPath}/" style="color: red ;">Home</a>
        
        <ul class="navbar-nav mr-2">
        <li class="nav-item dropdown">
        <a  class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"id="navbarDropdown"
              role="button"aria-haspopup="true"
              aria-expanded="false">Thong Tin khach hang</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="${pageContext.request.contextPath}/khachHang/HienThiKhachHang">Danh Sach khach hang</a>
            <a class="dropdown-item"href="${pageContext.request.contextPath}/khachHang/ThemKhachHang">Them moi khach hang</a>
          </div>
            </li>
        </ul>
        <ul class="navbar-nav mr-2">
            <li class="nav-item dropdown">
            <a  class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Dang Ky Ve Xe </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="${pageContext.request.contextPath}/datVe/HienThiDatVe">Danh Sach Dang Ky Ve</a>
            <a class="dropdown-item" href="${pageContext.request.contextPath}/datVe/DatVe">Dat ve</a>
             <a class="dropdown-item" href="${pageContext.request.contextPath}/khachHangDTO/HienThiTatCa">Hien thi tat ca</a>
          </div>
            </li>
        </ul>
    

        <form class="form-inline my-2 my-lg-0 ">
            <input
              class="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
              Search
            </button>
          </form>
    </div>
</div>

</body>
</html>