<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%-- <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet"/> --%>
<title>New san</title>
<style>
	.error{
	color: red;
	font-style: italic;
	}
</style>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/GiaoDien.jsp"></jsp:include>

<h3 align="center">THEM THONG TIN KHACH HANG</h3>

<div align="center">
		<form:form action="${pageContext.request.contextPath}/khachHang/save-khachHang" method="POST" modelAttribute="khachHangForm">
			<table>
				<tr>
					<td><form:label path="CCCD">CCCD</form:label></td>
					<td><form:input path="CCCD" name="CCCD" id="CCCD" /></td>
					<td><form:errors path="CCCD" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="hoTen" >Ten khach hang</form:label></td>
					<td><form:input path="hoTen" name="hoTen" id="hoTen" /></td>
					<td><form:errors path="hoTen" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="ngaySinh" >Ngay Sinh</form:label></td>
					<td><form:input type="date" path="ngaySinh" name="ngaySinh" id="ngaySinh" /></td>
					<td><form:errors path="ngaySinh" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="gioiTinh" >Gioi tinh</form:label></td>
					<td><form:input path="gioiTinh" name="gioiTinh" id="gioiTinh" /></td>
					<td><form:errors path="gioiTinh" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="soDienThoai" >SDT</form:label></td>
					<td><form:input path="soDienThoai" name="soDienThoai" id="soDienThoai" /></td>
					<td><form:errors path="soDienThoai" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="trangThaiTiem" >Trang thai tiem</form:label></td>
					<td><form:input path="trangThaiTiem" name="trangThaiTiem" id="trangThaiTiem" /></td>
					<td><form:errors path="trangThaiTiem" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="ketQuaXetNghiem" >Ket qua xet nghiem</form:label></td>
					<td><form:input path="ketQuaXetNghiem" name="ketQuaXetNghiem" id="ketQuaXetNghiem" /></td>
					<td><form:errors path="ketQuaXetNghiem" cssClass="error"/></td>
				</tr>
				<tr>
					<td></td>
					<td><form:button value="Add">Luu khach hang</form:button></td>
				</tr>
			</table>
		</form:form>
		</div>
</body>
</html>