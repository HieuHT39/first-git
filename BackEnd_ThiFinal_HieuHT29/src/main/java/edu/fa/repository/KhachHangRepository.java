package edu.fa.repository;

import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.fa.model.KhachHang;
import edu.fa.page.PageAple;

@Repository
public class KhachHangRepository {
	@Autowired
	private SessionFactory sessionFactory;

	// Lưu các data vào bảng hiênr thị khách hàng và tối đa 3 recod sẽ chuyển sang trang khác 
	public List<KhachHang> findWithPageAble(PageAple pageAble) {
		Session session = sessionFactory.getCurrentSession();
		List<KhachHang> khachHangs = session.createQuery("FROM KhachHang", KhachHang.class)
					.setFirstResult(pageAble.getOffset()).setMaxResults(pageAble.getSize()).getResultList();
		
		return khachHangs;
	}
	// Đếm số recod của DB
	public long count() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("SELECT COUNT(*) FROM KhachHang", Long.class).getSingleResult();
	}


	// Luu thong tin khach hang vao database
	@Transactional
	public void saveOrUpdate(KhachHang khachHang) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(khachHang);

	}
	// Luu thong tin khach hang vao database
	 @Transactional
	public void save(KhachHang khachHang) {
		Session session = sessionFactory.getCurrentSession();
		session.save(khachHang);
	}
	 // xoa data
	 public void delete(KhachHang khachHang) {
			Session session = sessionFactory.getCurrentSession();
			session.delete(khachHang);
			
		}
	 public KhachHang findById(String id) {
			Session session = sessionFactory.getCurrentSession();
			return session.find(KhachHang.class, id);
		}
	 // update data
	 
	 public void update(KhachHang khachHang) {
			Session session = sessionFactory.getCurrentSession();
			session.update(khachHang);
		}
	 // hien thi tata ca
		public List<Object[]> listTTKH() {

			Session session = sessionFactory.getCurrentSession();
			String hql = "SELECT dv.cccd, dv.khachHang.hoTen, dv.khachHang.ngaySinh, dv.khachHang.gioiTinh, dv.khachHang.soDienThoai,"
					+ " dv.khachHang.trangThaiTiem, dv.khachHang.ketQuaXetNghiem, dv.khachHang.ngayXetNghiem,"
					+ " dv.maDV, dv.ngayDatVe, dv.bienSoXe, dv.diaDiemXuatPhat, dv.diaDiemDen, dv.ngayXuatPhat"
					+ " FROM DatVe dv";
			
			Query query = session.createQuery(hql, Object[].class);
			List<Object[]> listKH = query.getResultList();
			
			return listKH;
		}
		
}
