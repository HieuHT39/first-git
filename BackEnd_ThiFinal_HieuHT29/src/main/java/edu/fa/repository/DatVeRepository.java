package edu.fa.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.fa.model.DatVe;
import edu.fa.page.PageAple;

@Repository
public class DatVeRepository {
	@Autowired
	private SessionFactory sessionFactory;
	
	// Luu thong tin khach hang vao database
	@Transactional
	public void saveOrUpdate(DatVe datVe) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(datVe);

	}
	// Hiển thị danh sách Dang ky
	public List<DatVe> findWithPageAble(PageAple pageAble) {
		Session session = sessionFactory.getCurrentSession();
		List<DatVe> datVes = session.createQuery("FROM DatVe", DatVe.class)
					.setFirstResult(pageAble.getOffset()).setMaxResults(pageAble.getSize()).getResultList();
		
		return datVes;
	}
	public long count() {
		Session session = sessionFactory.getCurrentSession();
		return session.createQuery("SELECT COUNT(*) FROM DatVe", Long.class).getSingleResult();
	}
	// xoa data
	 public void delete(DatVe datVe) {
			Session session = sessionFactory.getCurrentSession();
			session.delete(datVe);
			
		}
	 public DatVe findById(String id) {
			Session session = sessionFactory.getCurrentSession();
			return session.find(DatVe.class, id);
		}
	 // update data
	 
	 public void update(DatVe datVe) {
			Session session = sessionFactory.getCurrentSession();
			session.update(datVe);
		}
}
