package edu.fa.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.fa.model.DatVe;
import edu.fa.page.PageAple;
import edu.fa.service.DatVeService;
import edu.fa.service.KhachHangService;

@Controller
@RequestMapping("/datVe")
public class DatVeController {
	@Autowired
	private DatVeService datVeService;
	@Autowired
	private KhachHangService khachHangService;
	
	//Hien thi ra form Dang ky
		@GetMapping("/DatVe")
		public String showUseServiceRegisterForm(Model model) {
			model.addAttribute("datVeForm", new DatVe());
			return "DatVe";
		}

		@PostMapping("/save-datVe")
		public String saveNewService(@Validated @ModelAttribute(name = "datVeForm") @Valid DatVe datVe,
				BindingResult result,BindingResult bindingResult) {
//			// Validate ngay thue phai lon hon ngay hien tai
//			DangKyThueValidator valided = new DangKyThueValidator();
//			valided.validate(dangKyThue, result);
//			if (result.hasErrors()) {
//				return "ThueSan";
//			}
//			// Validate ma loai san khong ton tai
//			LoaiSan loaiSanValid = loaiSanService.findById(dangKyThue.getLoaiSan().getMaLoaiSan());
//			if (loaiSanValid == null) {
//				ObjectError error = new FieldError("dangKyThueForm", "loaiSan.maLoaiSan",
//						dangKyThue.getLoaiSan().getMaLoaiSan(), false, null, null, "ma san chua ton tai");
//			
//				bindingResult.addError(error);
//				return "ThueSan";
//			}

			datVeService.saveOrUpdate(datVe);
//		return "redirect:/service/list-service";
			return "HienThiDatVe";
//		return "list-service";
//		return "error-page";
		}
		
		// Hiển thị danh sách Dang ky
		@RequestMapping("/HienThiDatVe")
		public String getAllCustomerWithPageAble(Model model, @RequestParam(defaultValue = "1") Integer page) {
		PageAple pageAble = new PageAple(page);
		List<DatVe> datVes = datVeService.findWithPageAble(pageAble);
		//Hiển thị  danh sach 
		model.addAttribute("dv", datVes);
		// Chuyển trang , tìm kiếm theo trang 
		model.addAttribute("totalPages", datVeService.totalPages(pageAble));
		// trang hiện tại 
		model.addAttribute("currentPage", page);
			return "HienThiDatVe";
		}
//		@RequestMapping("/HienThiTatCa")
//		public String listfulinfo(Model model, @RequestParam(defaultValue = "1") Integer page) {
//		PageAple pageAble = new PageAple(page);//
//		List<DatVe> datves = datVeService.findWithPageAble(pageAble);//
//		model.addAttribute("dv", datves);
//		model.addAttribute("totalPages", datVeService.totalPages(pageAble));
//		model.addAttribute("currentPage", page);
//			return "HienThiTatCa";
//		}
		
		// xoa DB
		@RequestMapping("/delete")
		public String deleteSan(@RequestParam(name = "id") String cccd, Model model) {
			datVeService.delete(cccd);
			return "redirect:/datVe/HienThiDatVe";
		}
		// update DB
		@RequestMapping("/update/{id}")
		public String updateSan(@PathVariable(name = "id") String cccd, Model model) {
			DatVe datVe = datVeService.findById(cccd);
			model.addAttribute("datVeForm", datVe);
			return "DatVe";
		}
}
