package edu.fa.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import edu.fa.model.KhachHang;
import edu.fa.model.KhachHangDTO;
import edu.fa.page.PageAple;
import edu.fa.service.KhachHangService;


@Controller
@RequestMapping("/khachHang")
public class KhachHangController {
	@Autowired
	private KhachHangService khachHangService;
	// Hien thi mang hinh them khach hang
			@RequestMapping(value = "ThemKhachHang") // sử dụng để ánh xạ đến các phương thức xử lý trong spring MVC
			  public ModelAndView showLogin() {
				// ModelAndView dùng để trả về một giá trị bao gồm cả model và view 
				  ModelAndView mav = new ModelAndView("ThemKhachHang");
				  mav.addObject("khachHangForm", new KhachHang());
				  return mav;
			  }	
			
			@GetMapping("/listinfo")
			public String thongTinKhachHang(Model model, @RequestParam(defaultValue = "1") Integer page) {
				PageAple pageAble = new PageAple(page);
				List<KhachHangDTO> listKH = khachHangService.convertDTO();
				model.addAttribute("items", listKH);
				model.addAttribute("totalPages", khachHangService.totalPages(pageAble));
				// trang hiện tại 
				model.addAttribute("currentPage", page);
				return "HienThiTatCa";
				
			}
			
			// Luu thong tin khach hang
			
			@PostMapping("/save-khachHang")
			// dung @validated de xac thu cac rang buoc validate trong class KH
			// dung BindingResult result de luu cac qua trinh
			public String saveNewCustomer(@Validated @ModelAttribute(name = "khachHangForm") @Valid KhachHang khachHang, BindingResult result) {
				// Neu co xay ra thi tro lai phan nhap KH
				if(result.hasErrors()) {
					return "ThemKhachHang";
				}
				
				khachHangService.saveOrUpdate(khachHang);
//			
				return "redirect:/khachHang/HienThiKhachHang";
			}
			
			// Hiển thị danh sách khách hàng 
			@RequestMapping("/HienThiKhachHang")
			public String getAllCustomerWithPageAble(Model model, @RequestParam(defaultValue = "1") Integer page) {
			PageAple pageAble = new PageAple(page);
			List<KhachHang> khachHangs = khachHangService.findWithPageAble(pageAble);
			//Hiển thị khách hàng 
			model.addAttribute("khachHangs", khachHangs);
			// Chuyển trang , tìm kiếm theo trang 
			model.addAttribute("totalPages", khachHangService.totalPages(pageAble));
			// trang hiện tại 
			model.addAttribute("currentPage", page);
				return "HienThiKhachHang";
			}
			
			// xoa DB
			@RequestMapping("/delete-khachHang")
			public String deleteSan(@RequestParam(name = "id") String cccd, Model model) {
				khachHangService.delete(cccd);
				return "redirect:/khachHang/HienThiKhachHang";
			}
			// update DB
			@RequestMapping("/update-khachHang/{id}")
			public String updateSan(@PathVariable(name = "id") String cccd, Model model) {
				KhachHang khachHang = khachHangService.findById(cccd);
				model.addAttribute("khachHangForm",khachHang );
				return "ThemKhachHang";
			}
	
}
