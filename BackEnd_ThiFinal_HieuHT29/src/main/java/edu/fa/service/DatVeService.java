package edu.fa.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.fa.model.DatVe;
import edu.fa.page.PageAple;
import edu.fa.repository.DatVeRepository;


@Service
public class DatVeService {
	@Autowired
	private DatVeRepository datVeRepository;

	//luu thong tin khach hang vao DB

		public void saveOrUpdate(DatVe datVe) {
			datVeRepository.saveOrUpdate(datVe);
		}
		
		
	// Hiển thị danh sách Dang ky
		@Transactional
		public List<DatVe> findWithPageAble(PageAple pageAble) {
			return datVeRepository.findWithPageAble(pageAble);
		}
		 // Dùng để tổng số  trang , và hiển thị danh sách khách hâng 
		@Transactional
		public int totalPages(PageAple pageAble) {
			long totalRecord = datVeRepository.count();
			return (int) Math.ceil((double) totalRecord / pageAble.getSize());

			
		}
		
		// xoa DB
		
			@Transactional
			public void delete(String id) {
				datVeRepository.delete(datVeRepository.findById(id));

			}
			// update DB
			@Transactional
			public void update(DatVe datVe) {
				datVeRepository.update(datVe);
			}
			@Transactional
			public DatVe findById(String id) {
				return datVeRepository.findById(id);
			}
}
