package edu.fa.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import edu.fa.model.KhachHang;
import edu.fa.model.KhachHangDTO;
import edu.fa.page.PageAple;
import edu.fa.repository.KhachHangRepository;

@Service
public class KhachHangService {
	@Autowired
	private KhachHangRepository khachHangRepository;

	//Dùng để tìm kiếm theo trang , và hiển thị danh sách khách hâng 
		@Transactional
		public List<KhachHang> findWithPageAble(PageAple pageAble) {
			return khachHangRepository.findWithPageAble(pageAble);
		}
		 // Dùng để tổng số  trang , và hiển thị danh sách khách hâng 
		@Transactional
		public int totalPages(PageAple pageAble) {
			long totalRecord = khachHangRepository.count();
			return (int) Math.ceil((double) totalRecord / pageAble.getSize());

		}
		
		// luu thong tin khach hang vao DB
		
		public void saveOrUpdate(KhachHang khachHang) {
			khachHangRepository.saveOrUpdate(khachHang);
		}
		// luu thong tin khach hang vao DB
		public void save(KhachHang khachHang) {
			khachHangRepository.save(khachHang);
		}
		// xoa DB
		
		@Transactional
		public void delete(String id) {
			khachHangRepository.delete(khachHangRepository.findById(id));

		}
		// update DB
		@Transactional
		public void update(KhachHang khachHang) {
			khachHangRepository.update(khachHang);
		}
		@Transactional
		public KhachHang findById(String id) {
			return khachHangRepository.findById(id);
		}
		// hien thi tata  ca
		public List<KhachHangDTO> convertDTO() {

			List<Object[]> listKH = khachHangRepository.listTTKH();
			List<KhachHangDTO> khachHangDTOs = new ArrayList<>();
			
			for (Object[] o : listKH) {
				KhachHangDTO dto = new KhachHangDTO();

				dto.setCccd((String)o[0]);
				dto.setHoTen((String)o[1]);
				dto.setNgaySinh((LocalDate)o[2]);
				dto.setGioiTinh((String)o[3]);
				dto.setSoDienThoai((String)o[4]);
				dto.setTrangThaiTiem((String)o[5]);
				dto.setKetQuaXetNghiem((String)o[6]);
				dto.setNgayXetNghiem((LocalDate)o[7]);
				dto.setMaDV((String)o[8]);
				dto.setNgayDatVe((LocalDate)o[9]);
				dto.setBienSoXe((String)o[10]);
				dto.setDiaDiemXuatPhat((String)o[11]);
				dto.setDiaDiemDen((String)o[12]);
				dto.setNgayXuatPhat((LocalDate)o[13]);
				
				khachHangDTOs.add(dto);
			}
			
			return khachHangDTOs;
		}
}
