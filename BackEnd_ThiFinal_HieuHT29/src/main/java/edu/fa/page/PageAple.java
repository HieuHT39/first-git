package edu.fa.page;

public class PageAple {
	private int page;
	private int size = 3;
	public PageAple(int page) {
		super();
		this.page = page;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	public int getOffset() {
		return (page - 1) * size;
	}
	
}
