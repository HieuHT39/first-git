package edu.fa.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;
@Entity
public class DatVe {
@Id
private String maDV;
@ManyToOne
@JoinColumn(name = "CCCD")
private KhachHang CCCD;
@DateTimeFormat(pattern = "yyyy-MM-dd")
private LocalDate ngayDatVe;
private String bienSoXe;
private String diaDiemXuatPhat;
private String diaDiemDen;
@DateTimeFormat(pattern = "yyyy-MM-dd")
private LocalDate ngayXuatPhat;
public DatVe(String maDV, KhachHang cCCD, LocalDate ngayDatVe, String bienSoXe, String diaDiemXuatPhat,
		String diaDiemDen, LocalDate ngayXuatPhat) {
	super();
	this.maDV = maDV;
	CCCD = cCCD;
	this.ngayDatVe = ngayDatVe;
	this.bienSoXe = bienSoXe;
	this.diaDiemXuatPhat = diaDiemXuatPhat;
	this.diaDiemDen = diaDiemDen;
	this.ngayXuatPhat = ngayXuatPhat;
}
public DatVe() {
	super();
	// TODO Auto-generated constructor stub
}
public String getMaDV() {
	return maDV;
}
public void setMaDV(String maDV) {
	this.maDV = maDV;
}
public KhachHang getCCCD() {
	return CCCD;
}
public void setCCCD(KhachHang cCCD) {
	CCCD = cCCD;
}
public LocalDate getNgayDatVe() {
	return ngayDatVe;
}
public void setNgayDatVe(LocalDate ngayDatVe) {
	this.ngayDatVe = ngayDatVe;
}
public String getBienSoXe() {
	return bienSoXe;
}
public void setBienSoXe(String bienSoXe) {
	this.bienSoXe = bienSoXe;
}
public String getDiaDiemXuatPhat() {
	return diaDiemXuatPhat;
}
public void setDiaDiemXuatPhat(String diaDiemXuatPhat) {
	this.diaDiemXuatPhat = diaDiemXuatPhat;
}
public String getDiaDiemDen() {
	return diaDiemDen;
}
public void setDiaDiemDen(String diaDiemDen) {
	this.diaDiemDen = diaDiemDen;
}
public LocalDate getNgayXuatPhat() {
	return ngayXuatPhat;
}
public void setNgayXuatPhat(LocalDate ngayXuatPhat) {
	this.ngayXuatPhat = ngayXuatPhat;
}

}
