package edu.fa.model;

import java.time.LocalDate;

public class KhachHangDTO {
	private String cccd;
	private String hoTen;
	private LocalDate ngaySinh;
	private String gioiTinh;
	private String soDienThoai;
	private String trangThaiTiem;
	private String ketQuaXetNghiem;
	private LocalDate ngayXetNghiem;

	private String maDV;
	private LocalDate ngayDatVe;
	private String bienSoXe;
	private String diaDiemXuatPhat;
	private String diaDiemDen;
	private LocalDate ngayXuatPhat;
	public KhachHangDTO(String cccd, String hoTen, LocalDate ngaySinh, String gioiTinh, String soDienThoai,
			String trangThaiTiem, String ketQuaXetNghiem, LocalDate ngayXetNghiem, String maDV, LocalDate ngayDatVe,
			String bienSoXe, String diaDiemXuatPhat, String diaDiemDen, LocalDate ngayXuatPhat) {
		super();
		this.cccd = cccd;
		this.hoTen = hoTen;
		this.ngaySinh = ngaySinh;
		this.gioiTinh = gioiTinh;
		this.soDienThoai = soDienThoai;
		this.trangThaiTiem = trangThaiTiem;
		this.ketQuaXetNghiem = ketQuaXetNghiem;
		this.ngayXetNghiem = ngayXetNghiem;
		this.maDV = maDV;
		this.ngayDatVe = ngayDatVe;
		this.bienSoXe = bienSoXe;
		this.diaDiemXuatPhat = diaDiemXuatPhat;
		this.diaDiemDen = diaDiemDen;
		this.ngayXuatPhat = ngayXuatPhat;
	}
	public KhachHangDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getCccd() {
		return cccd;
	}
	public void setCccd(String cccd) {
		this.cccd = cccd;
	}
	public String getHoTen() {
		return hoTen;
	}
	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}
	public LocalDate getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(LocalDate ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public String getSoDienThoai() {
		return soDienThoai;
	}
	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}
	public String getTrangThaiTiem() {
		return trangThaiTiem;
	}
	public void setTrangThaiTiem(String trangThaiTiem) {
		this.trangThaiTiem = trangThaiTiem;
	}
	public String getKetQuaXetNghiem() {
		return ketQuaXetNghiem;
	}
	public void setKetQuaXetNghiem(String ketQuaXetNghiem) {
		this.ketQuaXetNghiem = ketQuaXetNghiem;
	}
	public LocalDate getNgayXetNghiem() {
		return ngayXetNghiem;
	}
	public void setNgayXetNghiem(LocalDate ngayXetNghiem) {
		this.ngayXetNghiem = ngayXetNghiem;
	}
	public String getMaDV() {
		return maDV;
	}
	public void setMaDV(String maDV) {
		this.maDV = maDV;
	}
	public LocalDate getNgayDatVe() {
		return ngayDatVe;
	}
	public void setNgayDatVe(LocalDate ngayDatVe) {
		this.ngayDatVe = ngayDatVe;
	}
	public String getBienSoXe() {
		return bienSoXe;
	}
	public void setBienSoXe(String bienSoXe) {
		this.bienSoXe = bienSoXe;
	}
	public String getDiaDiemXuatPhat() {
		return diaDiemXuatPhat;
	}
	public void setDiaDiemXuatPhat(String diaDiemXuatPhat) {
		this.diaDiemXuatPhat = diaDiemXuatPhat;
	}
	public String getDiaDiemDen() {
		return diaDiemDen;
	}
	public void setDiaDiemDen(String diaDiemDen) {
		this.diaDiemDen = diaDiemDen;
	}
	public LocalDate getNgayXuatPhat() {
		return ngayXuatPhat;
	}
	public void setNgayXuatPhat(LocalDate ngayXuatPhat) {
		this.ngayXuatPhat = ngayXuatPhat;
	}
	
}
