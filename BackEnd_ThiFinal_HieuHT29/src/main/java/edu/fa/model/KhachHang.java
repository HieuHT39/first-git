package edu.fa.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.swing.table.TableStringConverter;

import org.springframework.format.annotation.DateTimeFormat;
@Entity
public class KhachHang {
@Id
 private String CCCD;
 private String hoTen;
@DateTimeFormat(pattern = "yyyy-MM-dd")
 private LocalDate ngaySinh;
 private String gioiTinh;
 private String soDienThoai;
 private String trangThaiTiem;
 private String ketQuaXetNghiem;
 @DateTimeFormat(pattern = "yyyy-MM-dd")
 private LocalDate ngayXetNghiem;
 @OneToMany(mappedBy = "CCCD")
 private List<DatVe> datVes ;

public KhachHang() {
	super();
	// TODO Auto-generated constructor stub
}

public KhachHang(String cCCD, String hoTen, LocalDate ngaySinh, String gioiTinh, String soDienThoai,
		String trangThaiTiem, String ketQuaXetNghiem, LocalDate ngayXetNghiem, List<DatVe> datVes) {
	super();
	CCCD = cCCD;
	this.hoTen = hoTen;
	this.ngaySinh = ngaySinh;
	this.gioiTinh = gioiTinh;
	this.soDienThoai = soDienThoai;
	this.trangThaiTiem = trangThaiTiem;
	this.ketQuaXetNghiem = ketQuaXetNghiem;
	this.ngayXetNghiem = ngayXetNghiem;
	this.datVes = datVes;
}

public String getCCCD() {
	return CCCD;
}

public void setCCCD(String cCCD) {
	CCCD = cCCD;
}

public String getHoTen() {
	return hoTen;
}

public void setHoTen(String hoTen) {
	this.hoTen = hoTen;
}

public LocalDate getNgaySinh() {
	return ngaySinh;
}

public void setNgaySinh(LocalDate ngaySinh) {
	this.ngaySinh = ngaySinh;
}

public String getGioiTinh() {
	return gioiTinh;
}

public void setGioiTinh(String gioiTinh) {
	this.gioiTinh = gioiTinh;
}

public String getSoDienThoai() {
	return soDienThoai;
}

public void setSoDienThoai(String soDienThoai) {
	this.soDienThoai = soDienThoai;
}

public String getTrangThaiTiem() {
	return trangThaiTiem;
}

public void setTrangThaiTiem(String trangThaiTiem) {
	this.trangThaiTiem = trangThaiTiem;
}

public String getKetQuaXetNghiem() {
	return ketQuaXetNghiem;
}

public void setKetQuaXetNghiem(String ketQuaXetNghiem) {
	this.ketQuaXetNghiem = ketQuaXetNghiem;
}

public LocalDate getNgayXetNghiem() {
	return ngayXetNghiem;
}

public void setNgayXetNghiem(LocalDate ngayXetNghiem) {
	this.ngayXetNghiem = ngayXetNghiem;
}

public List<DatVe> getDatVes() {
	return datVes;
}

public void setDatVes(List<DatVe> datVes) {
	this.datVes = datVes;
}
 
 
}
